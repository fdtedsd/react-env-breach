import React from 'react';
import logo from './logo.svg';
import './App.css';

function payload(str) {
	return str || process.env.SECRET;
}

function App() {
	return (
		<div className="App">
			<header className="App-header">
				<img src={logo} className="App-logo" alt="logo" />
				<p>{payload('Hello world')}</p>
				<a className="App-link" href="https://reactjs.org" target="_blank" rel="noopener noreferrer">
					Learn React
				</a>
			</header>
		</div>
	);
}

export default App;
